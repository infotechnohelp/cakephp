<?php

use Cake\Core\Configure;

Configure::write('PRINT_DEBUG', filter_var(getenv('PRINT_DEBUG'), FILTER_VALIDATE_BOOLEAN));

Configure::write('SESSION_DEBUG', filter_var(getenv('SESSION_DEBUG'), FILTER_VALIDATE_BOOLEAN));

/** @var \App\Application $Application */
$Application = new \App\Application(CONFIG);

$Application->addPlugin('ApiHandler');

$Application->addPlugin('Languages');