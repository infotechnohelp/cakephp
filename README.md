## cakephp-languages

* Create JS locale files in `webroot/js/Locale/en_US.js`

Terminal: `bin/cake locale js` (All files) || `bin/cake locale js en_US` (Concrete language)

* LocaleManager

`src/Locale` contains `et_EE`, `en_US` is a default locale

`LocaleManager::getLanguageCodes()` => `['en_US', 'et_EE']`

`LocaleManager::getLanguageLabels()` => `['ENG', 'EST']`

`LocaleManager::getLanguageLabels(['en_US' => 'US'])` => `['US', 'EST']`

* LanguagesComponent

`$controller->loadComponent('Languages.Languages')` sets cookie `language` and I18n locale 

Set language cookie and I18n locale with GET query parameter `lang=en_US` or writing directly to `Cookie.language`

* Language routing

`demo` redirects to `en/demo` (Default locale is `en_US`)

`demo?lang=et_EE` redirects to `et/demo?lang=et_EE`

`en/demo?lang=et_EE` redirects to `et/demo?lang=et_EE`


