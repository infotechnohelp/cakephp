<?php
if (\Cake\Core\Configure::read('SESSION_DEBUG')) {
    $this->getRequest()->getSession()->write('SESSION_DEBUG', sprintf('%s%s',
        $this->getRequest()->getSession()->read('SESSION_DEBUG'), 'CoreDefaultLayout;'));
}
?>

<h3>Core default layout</h3>

<?= $this->fetch('content') ?>