<?php

namespace Core\Exceptions;

/**
 * Class MyPluginException
 * @package Core\Exceptions
 */
class MyPluginException extends \Exception
{
    /**
     * PathCellValidationException constructor.
     *
     * @param string $message
     */
    public function __construct($message = "")
    {
        parent::__construct($message);
    }
}
