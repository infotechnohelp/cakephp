<?php

declare(strict_types=1);

namespace Core\Controller;

use \Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Languages\Lib\LocaleManager;

/**
 * Class AppController
 * @package Core\Controller
 */
class AppController extends Controller
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Cookie');

        $this->loadComponent('Languages.Languages');

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);

        if (Configure::read('PRINT_DEBUG')) {
            debug('CoreController');
            debug(LocaleManager::getLanguageLabels(['en_US' => 'US']));
        }

        if (Configure::read('SESSION_DEBUG')) {
            $this->getRequest()->getSession()->write('SESSION_DEBUG', 'CoreController;');
        }
    }

    public function beforeFilter(Event $event)
    {
        if (Configure::read('PRINT_DEBUG')) {
            debug($this->Cookie->read('language'));
            debug(I18n::getLocale());
        }
    }
}
