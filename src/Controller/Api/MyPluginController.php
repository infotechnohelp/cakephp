<?php

namespace Core\Controller\Api;

use Cake\ORM\TableRegistry;
use Core\Exceptions\MyPluginException;

/**
 * Class MyPluginController
 * @package Core\Controller\Api
 */
class MyPluginController extends AppController
{
    /**
     * @return \Cake\Http\Response
     */
    public function getById()
    {
        return $this->response->withStringBody(
            json_encode(TableRegistry::get('MyPlugins')->get($this->request->getData('id')))
        );
    }

    /**
     * @throws \Core\Exceptions\MyPluginException
     */
    public function exception()
    {
        throw new MyPluginException('Exception');
    }
}
