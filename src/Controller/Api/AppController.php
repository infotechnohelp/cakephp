<?php

declare(strict_types=1);

namespace Core\Controller\Api;

use ApiHandler\Traits\ApiController;
use App\Controller\AppController as BaseController;
use Cake\Event\Event;
use Cake\I18n\I18n;

/**
 * Class AppController
 * @package Core\Controller\Api
 */
class AppController extends BaseController
{
    use ApiController;

    public function beforeFilter(Event $event)
    {
        I18n::setLocale($this->Cookie->read('language'));
    }
}
