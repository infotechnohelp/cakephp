<?php
namespace Core\Model\Entity;

use Cake\ORM\Entity;

/**
 * Class MyPlugin
 * @package Core\Model\Entity
 */
class MyPlugin extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    // @codingStandardsIgnoreEnd
}
