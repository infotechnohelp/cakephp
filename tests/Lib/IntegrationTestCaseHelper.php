<?php

namespace Core\Test\Lib;
/**
 * Trait IntegrationTestCaseHelper
 * @package Core\Test\Lib
 */
trait IntegrationTestCaseHelper
{
    /**
     * @return mixed
     */
    private function _getJsonResponse()
    {
        return json_decode((string)$this->_response->getBody(), true);
    }

    /**
     * @return mixed
     */
    private function _getLocationHeader()
    {
        return $this->_response->getHeader('Location')[0];
    }
}